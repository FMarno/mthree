PORTS:
ClientManager - 2050
TraderManager - 2051
RouterManager - 2052

** Protocol **
Client -> ClientManager
New Order Single
11 | Volume | Instrument

Trader -> TraderManager
accept order
12 | id
slice order
14 | id | volume

Router -> RouterManager
best price
15 | id | sliceid | price
new fill
16 | id | sliceid | volume | price

TraderManager -> Trader
new order
0 | id | volume | instrument
price
1 | id | price
cross
2 | id | volume
fill
3 | id | volume

ClientManager -> Client
order pending
4 | id
order accepted
5 | id
order declined
6 | id
fill
7 | id | volume
complete
8 | id

Router Manager -> Router
route order
9 | id | sliceid | volume | instrument
price at size
10 | id | sliceid | volume | instrument

Order.Status {NEW,ACCEPTED,REJECTED,PENDING_FILL,PARTIAL_FILL,FILLED, FAILED, PRICED};