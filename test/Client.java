import OrderClient.OrderClientGroup;

import java.net.InetSocketAddress;

/**
 * Created by alumniCurie15 on 05/07/2017.
 */
public class Client {
    public static void main(String[] args){
        OrderClientGroup group = new OrderClientGroup(10, new InetSocketAddress("localhost", 2050), 6,10,0);
        Thread t = new Thread(group);
        t.start();
    }
}
