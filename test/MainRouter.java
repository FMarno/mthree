import OrderRouter.OrderRouterGroup;

import java.net.InetSocketAddress;

/**
 * Created by Naeem on 06/07/2017.
 */
public class MainRouter {
    public static void main(String[] args){
        OrderRouterGroup orderRouterGroup = new OrderRouterGroup(2, new InetSocketAddress("localhost", 2052),10,0);
        Thread t = new Thread(orderRouterGroup);
        t.start();
    }
}
