import OrderTrader.OrderTraderGroup;

import java.net.InetSocketAddress;

/**
 * Created by Naeem on 06/07/2017.
 */
public class MainTrader {
    public static void main(String[] args){
        OrderTraderGroup traderGroup = new OrderTraderGroup(2, new InetSocketAddress("localhost", 2051),10,0);
        Thread t = new Thread(traderGroup);
        t.start();
    }
}
