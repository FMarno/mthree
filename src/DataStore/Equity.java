package DataStore;

import Builders.SharePricing;

import java.util.ArrayList;

/**
 * Created by James on 7/6/2017.
 */
public class Equity
{
	//Naming
	private String fullName;
	private String shortName;
	private String isin;
	
	//Pricing
	private ArrayList<Float> priceHistory;
	private Float openPrice;
	private Float closePrice;
	
	//Statistics
	private Float dayHigh;
	private Float dayLow;
	private Float dayAverage;
	
	//TODO price fluctuation reference
	private SharePricing sharePricing;
	
	public Equity(String fullName, String shortName, String isin)
	{
		this.fullName = fullName;
		this.isin = fullName;
		this.isin = isin;
		sharePricing = new SharePricing();
		
		closePrice = sharePricing.closePrice();
		openPrice = sharePricing.openPrice(closePrice);
		priceHistory = new ArrayList<>();
		priceHistory.add(sharePricing.fluctuatePrice(openPrice));
		for(int i = 0; i < 11; i++)
		{
			priceHistory.add(priceHistory.get(i)); // Probably concurrent exception / error
		}
	}
	
	public String getFullName()
	{
		return fullName;
	}
	
	public String getShortName()
	{
		return shortName;
	}
	
	public String getIsin()
	{
		return isin;
	}
	
	public Float getOpenPrice()
	{
		return openPrice;
	}
	
	public Float getClosePrice()
	{
		return  closePrice;
	}
	
	public ArrayList<Float> getPriceHistory ()
	{
		return priceHistory;
	}
	
	public Float getDayHigh ()
	{
		return dayHigh;
	}
	
	public Float getDayLow ()
	{
		return dayLow;
	}
	
	public Float getDayAverage ()
	{
		return dayAverage;
	}
}
/*

 */