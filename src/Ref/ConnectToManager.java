package Ref;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;

/**
 * Created by Naeem on 06/07/2017.
 */
public class ConnectToManager {

    /**
     * Connect to the Manager server specified by the ManagerAddress
     *
     * @return a blocking socket connected to the specified Manager or null if fails
     */
    public SocketChannel connect(InetSocketAddress ManagerAddress) throws InterruptedException {
        try {
            SocketChannel socketChannel;
            socketChannel = SocketChannel.open();
            socketChannel.configureBlocking(false);
            socketChannel.connect(ManagerAddress);
            while (!socketChannel.finishConnect()) {
                // System.out.println("waiting to connect");
                Thread.sleep(10);
            }
            // connection found
            return socketChannel;
        } catch (IOException e) {
            System.err.println("could not connect");
            e.printStackTrace();
        }
        return null;
    }
}
