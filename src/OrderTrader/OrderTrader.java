package OrderTrader;

import OrderClient.NewOrderSingle;
import Managers.Order;
import Ref.ConnectToManager;
import Ref.Instrument;
import Ref.Ric;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Random;

/**
 * Created by Naeem on 06/07/2017.
 */
public class OrderTrader extends ConnectToManager implements Trader,Runnable {
    private final InetSocketAddress traderManagerAddress;
    private final  int milli,nano;
    private SocketChannel channel;



    public OrderTrader(InetSocketAddress TraderManagerAddress, int milli, int nano) {
        this.traderManagerAddress=TraderManagerAddress;
        this.milli=milli;
        this.nano=nano;
        try {
            this.channel = connect(this.traderManagerAddress);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void run() {
        Random rand = new Random();
        for (int i = 0; i < 1; i++) {
            int size = rand.nextInt();
            float price = rand.nextFloat();
            Instrument instrument = new Instrument(new Ric("FIN.LAY"));
            NewOrderSingle order = new NewOrderSingle(size, price, instrument);
            System.out.println("sending");
            try {
                int check = this.newOrder(1,"this is a new order");
                if (check == -1){
                    System.err.println("error in newOrder");
                }
                Thread.sleep(milli, nano);
            } catch (IOException | InterruptedException e) {
                System.err.println("Client threw exception");
                e.printStackTrace();
                return;
            }
        }
        while (true){}
    }

    @Override
    public int newOrder(int id, String order) throws IOException, InterruptedException {
        if (channel == null && !channel.isConnected()) {
            System.err.println("not connected to Trader Manager");
            return -1;
        }
        //String test = "I am testing this";
        //int messageLen = test.getBytes().length;
        //System.out.println("id=" + id + "|" + order);

        ByteBuffer byteBuffer = ByteBuffer.allocate(order.getBytes().length);
        byteBuffer.put(order.getBytes());
        byteBuffer.flip();
        while (byteBuffer.hasRemaining()) {
            channel.write(byteBuffer);
        }
        byteBuffer.clear();

        ByteBuffer responseMessage = ByteBuffer.allocate(48 * 48);
        boolean wait = false;
        int bytesRead = 0;
        String message = null;
        System.out.println("Waiting for msg");
        while (!wait) {
            bytesRead += channel.read(responseMessage);
            message = new String(responseMessage.array());
            if (bytesRead > 0) {
                wait = true;
            }
        }
        System.out.println(message);

        return 0;
    }

    @Override
    public void acceptOrder(int id) throws IOException {

    }

    @Override
    public void sliceOrder(int id, int sliceSize) throws IOException {

    }

    @Override
    public void price(int id, Order o) throws InterruptedException, IOException {

    }

}
