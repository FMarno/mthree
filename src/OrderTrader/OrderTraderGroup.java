package OrderTrader;

import java.net.InetSocketAddress;

/**
 * Created by Naeem on 06/07/2017.
 */
public class OrderTraderGroup implements Runnable {
    private final OrderTrader[] traders;
    private final int numTraders;

    public OrderTraderGroup(int numTraders, InetSocketAddress localhost, int milli, int nano){
        this.numTraders = numTraders;
        this.traders = new OrderTrader[numTraders];
        for (int i=0; i<numTraders;i++){
            traders[i] = new OrderTrader(localhost,milli,nano);
        }
    }

    @Override
    public void run() {
        for(OrderTrader trader : traders) new Thread(trader).start();
        System.out.println(this.numTraders + " Started");
    }
}
