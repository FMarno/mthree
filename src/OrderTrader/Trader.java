package OrderTrader;

import Managers.Order;

import java.io.IOException;

/**
 * Created by Naeem on 06/07/2017.
 */
public interface Trader {

    public int newOrder(int id, String order) throws IOException, InterruptedException;

    public void acceptOrder(int id) throws IOException;


    public void sliceOrder(int id, int sliceSize) throws IOException;

    public void price(int id,Order o) throws InterruptedException, IOException;
}
