package OrderClient;

import Ref.ConnectToManager;
import Ref.Instrument;
import Ref.Ric;
import Managers.Order;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Random;

public class OrderClient extends ConnectToManager implements Client, Runnable {
    private final InetSocketAddress clientManagerAddress;
    private final int orders,nano;
    private final long milli;
    private SocketChannel channel;

    public OrderClient(InetSocketAddress clientManager, int orders, long milli, int nano) {
        this.clientManagerAddress = clientManager;
        this.orders = orders;
        this.milli = milli;
        this.nano = nano;
        try {
            this.channel = connect(this.clientManagerAddress);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        Random rand = new Random();
        for (int i = 0; i < 2; i++) {
            int size = rand.nextInt();
            float price = rand.nextFloat();
            Instrument instrument = new Instrument(new Ric("FIN.LAY"));
            NewOrderSingle order = new NewOrderSingle(size, price, instrument);
            System.out.println("sending");
            try {
                int check = this.sendOrder(order);
                if (check == -1){
                    System.err.println("error in sendOrder");
                }
                Thread.sleep(milli, nano);
            } catch (IOException | InterruptedException e) {
                System.err.println("Client threw exception");
                e.printStackTrace();
                return;
            }
        }
        while (true){}
    }

    /**
     * Will send the string "hello" to the clientManager
     *
     * @param order
     * @return
     * @throws IOException
     */
    @Override
    public int sendOrder(NewOrderSingle order) throws IOException {
        if (channel == null && !channel.isConnected()) {
            System.err.println("not connected to client manager");
            return -1;
        }
        String test = "10|1000|Some";
        int messageLen = test.getBytes().length;
        //System.out.println("id=" + id + "|" + order);

        ByteBuffer byteBuffer = ByteBuffer.allocate(messageLen);
        byteBuffer.put(test.getBytes());
        byteBuffer.flip();
        while (byteBuffer.hasRemaining()) {
            channel.write(byteBuffer);
        }
        byteBuffer.clear();

        ByteBuffer responseMessage = ByteBuffer.allocate(48 * 48);
        boolean wait = false;
        int bytesRead = 0;
        String message = null;
        System.out.println("Waiting for msg");
        while (!wait) {
            bytesRead += channel.read(responseMessage);
            message = new String(responseMessage.array());
            if (bytesRead > 0) {
                wait = true;
            }
        }
        System.out.println(message);

        return 0;
    }

    @Override
    public void sendCancel(int id) {

    }


    @Override
    public void messageHandler() {

    }

    @Override
    public void partialFill(Order order) {

    }

    @Override
    public void fullyFilled(Order order) {

    }

    @Override
    public void cancelled(Order order) {

    }

}
