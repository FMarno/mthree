package OrderClient;

import java.io.Serializable;

import Ref.Instrument;

public class NewOrderSingle implements Serializable{
	public int size;
	public float price;
	public Instrument instrument;
	public NewOrderSingle(int size,float price,Instrument instrument){
		this.size=size;
		this.price=price;
		this.instrument=instrument;
	}
	public NewOrderSingle(String str)
    {
        String[] temp = str.split("|");
        this.size = Integer.parseInt(temp[0]);
        this.price = Float.parseFloat(temp[1]);
        this.instrument = new Instrument(temp[2]); // TODO implement constructor body for instrument
    }

	public String toString(){
		return size+"|"+price+"|"+instrument;
	}
}