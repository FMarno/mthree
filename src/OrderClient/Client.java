package OrderClient;

import Managers.Order;

import java.io.IOException;

public interface Client{
	//Outgoing messages
	int sendOrder(NewOrderSingle order) throws IOException, InterruptedException;
	void sendCancel(int id);
	
	//Incoming messages
	void partialFill(Order order);
	void fullyFilled(Order order);
	void cancelled(Order order);
	
	void messageHandler();
}