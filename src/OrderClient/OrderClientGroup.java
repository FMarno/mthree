package OrderClient;

import java.net.InetSocketAddress;

/**
 * Created by alumniCurie15 on 05/07/2017.
 */
public class OrderClientGroup implements Runnable {
    private final OrderClient[] clients;
    private final int numClients;

    public OrderClientGroup(int numClients, InetSocketAddress address, int orders, long milli, int nano) {
        this.numClients = numClients;
        clients = new OrderClient[numClients];
        for (int i = 0; i < numClients; i++) {
            clients[i] = new OrderClient(address,orders, milli, nano);
        }
    }

    @Override
    public void run() {
        for (OrderClient oc : clients) {
            new Thread(oc).start();
        }
        System.out.println("clients started");
    }


}
