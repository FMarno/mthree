package Builders;

import java.util.Random;

/**
 * Created by James on 7/7/2017.
 */
public class SharePricing
{
	private static Random r = new Random();
	//TODO bunch of vars and logic for intelligent data simulation
	private Float modifier;
	
	public SharePricing()
	{
		modifier = 1f;
	}
	
	public Float closePrice()
	{
		return 2*r.nextFloat();
	}
	
	public Float openPrice(Float closePrice)
	{
		return closePrice + (r.nextFloat()*0.1f);
	}
	
	public Float fluctuatePrice(Float previousPrice)
	{
		if(Math.random() > 0.9)
		{
			modifier = modifier + 0.15f * r.nextFloat();
		}
		
		return previousPrice + r.nextFloat() * 0.1f * modifier;
	}
}
