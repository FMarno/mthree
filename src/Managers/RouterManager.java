package Managers;

import Ref.Instrument;

import java.io.IOException;
import java.nio.channels.SelectionKey;

public class RouterManager extends ParentManager {

    public RouterManager()
    {
        super(2052, Queues.getRMQueue());
        //messageHandler(2052);
    }

    @Override
    public void inbound(SelectionKey key, String s)
    {

        Order order = new Order(2, new Instrument("InstrumentTest"), Long.parseLong("123456"), Order.Status.NEW);
        order.setClientKey(key);
        Queues.addOrder(2,order);
        Queues.addToRMQueue(order);
        //15 | id | sliceid | price
//        String[] orderData = s.split("|");
//        switch (orderData[0]){
//            case "15":
//                int orderID = Integer.parseInt(orderData[1]);
//                int sliceID = Integer.parseInt(orderData[2]);
//                //get the list of slices TODO: 07/07/2017 Waiting on James to allow for getting the slice ID
//                //dummy best price
//
//                break;
//            case "16":
//                break;
//        }
        System.out.println(s);
    }

    @Override
    public String outbound(SelectionKey key, Order order)
    {
//        Order o=null;
//        while (true){
//            if (!Queues.getRMQueue().isEmpty()){
//                o=Queues.getRMQueue().poll();
//                break;
//            }
//        }
//        o.increment_bestPriceCounter();
//        switch (o.getStatus()){
//
//        }
        return "5|25|vod.l|25.0";
    }
    
    

    private void newFill(int id,int sliceId,int size,double price) throws IOException{
//        Order o=orders.get(id);
//        o.slices.get(sliceId).createFill(size, price);
//        if(o.sizeRemaining()==0){
//            Database.write(o);
//        }
//        sendOrderToTrader(id, o, TradeScreen.api.fill);
    }

    private void reallyRouteOrder(int sliceId,Order o) throws IOException{
        //TODO this assumes we are buying rather than selling
//        int minIndex=0;
//        double min=o.bestPrices[0];
//        for(int i=1;i<o.bestPrices.length;i++){
//            if(min>o.bestPrices[i]){
//                minIndex=i;
//                min=o.bestPrices[i];
//            }
//        }
//        ObjectOutputStream os=new ObjectOutputStream(orderRouters[minIndex].getOutputStream());
//        os.writeObject(Router.api.routeOrder);
//        os.writeInt(o.id);
//        os.writeInt(sliceId);
//        os.writeInt(o.sizeRemaining());
//        os.writeObject(o.instrument);
//        os.flush();
    }
	
	//==========================================================================//
    //Below this is for JAMES AND THE GUI PLEASE DON'T WRITE ANY CODE BELOW THIS//
	//==========================================================================//
	
	public void getData()
	{
	
	}
}
