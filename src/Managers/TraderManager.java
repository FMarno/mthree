package Managers;


import java.nio.channels.SelectionKey;
import java.util.Random;

/**
 * Created by alumniCurie15 on 06/07/2017.
 */
public class TraderManager extends ParentManager {
    Random rand = new Random();
    public TraderManager() {
        super(2051, Queues.getTMQueue());
    }

    @Override
    public void inbound(SelectionKey key, String s) {
        String[] split = s.split("|");
        switch (split[0]) {
            case "12":
                acceptOrder(Long.parseLong(split[1]));
                break;
            case "14":
                sliceOrder(Long.parseLong(split[1]), Integer.parseInt(split[2]));
                break;
            default:
                System.err.println("Trader Manager, invalid msgtype");
        }
    }

    @Override
    public String outbound(SelectionKey key,Order order) {
        Order.Status status = order.getStatus();
        switch (status){
            case PRICED:
                return "1|" + order.getId() + "|" + order.getPrice();
            case NEW:
                return "0|" + order.getId() + "|" + order.getVolume() + "|" + order.getInstrument();
            case FILLED:
            case PARTIAL_FILL:
                Slice slice = order.getSlices().get(order.getSlices().size()-1);
                return "3|" + order.getId() + "|" + slice.getVolume();
            default:
                System.err.println("Unexpected order status on trader stack: " + status);
        }
        return null;
    }

    public void acceptOrder(long id) {
        // ask Client manager to notify client
        Order order = Queues.getOrder(id);
        order.setStatus(Order.Status.ACCEPTED);
        Queues.addToCMQueue(order);
        //generate random price
        //order.setPrice((float)(Math.round(rand.nextFloat() *100)/100.0)); //TODO get the price
        Queues.addToRMQueue(order);

    }

    public void sliceOrder(long id, int sliceSize) {
        // find order
        Managers.Order order = Queues.getOrder(id);
        int sliceID = order.getSlices().size() -1;
        order.addSlice(new Slice(sliceID, sliceSize));
        order.setStatus(Order.Status.PENDING_FILL);
        Queues.addToRMQueue(order);

//        Order o=orders.get(id);
//        //slice the order. We have to check this is a valid size.
//        //Order has a list of slices, and a list of fills, each slice is a childorder and each fill is associated with either a child order or the original order
//        if(sliceSize>o.sizeRemaining()-o.sliceSizes()){
//            System.out.println("error sliceSize is bigger than remaining size to be filled on the order");
//            return;
//        }
//        int sliceId=o.newSlice(sliceSize);
//        Order slice=o.slices.get(sliceId);
//        internalCross(id,slice);
//        int sizeRemaining=o.slices.get(sliceId).sizeRemaining();
//        if(sizeRemaining>0){
//            routeOrder(id,sliceId,sizeRemaining,slice);
//        }
    }

//    public void priced(Order order){
//
//    }

//    private void routeOrder(int id, int sliceId, int size, Order order) {
////        for(Socket r:orderRouters){
////            ObjectOutputStream os=new ObjectOutputStream(r.getOutputStream());
////            os.writeObject(Router.api.priceAtSize);
////            os.writeInt(id);
////            os.writeInt(sliceId);
////            os.writeObject(order.instrument);
////            os.writeInt(order.sizeRemaining());
////            os.flush();
////        }
////        //need to wait for these prices to come back before routing
////        order.bestPrices=new double[orderRouters.length];
////        order.bestPriceCount=0;
//    }
//
//    private void internalCross(int id, Order o) {
////        for(Map.Entry<Integer, Order>entry:orders.entrySet()){
////            if(entry.getKey().intValue()==id)continue;
////            Order matchingOrder=entry.getValue();
////            if(!(matchingOrder.instrument.equals(o.instrument)&&matchingOrder.initialMarketPrice==o.initialMarketPrice))continue;
////            //TODO add support here and in Order for limit orders
////            int sizeBefore=o.sizeRemaining();
////            o.cross(matchingOrder);
////            if(sizeBefore!=o.sizeRemaining()){
////                sendOrderToTrader(id, o, TradeScreen.api.cross);
////            }
////        }
//    }


}
