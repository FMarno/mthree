package Managers;

import Ref.Instrument;

import java.nio.channels.SelectionKey;



public class ClientManager extends ParentManager {

    private long orderId = 0;

    public ClientManager() {
        super(2050, Queues.getCMQueue());

    }

    @Override
    public void inbound(SelectionKey key, String s) {


        String[] messageSplit = s.split("|");
        Order order = new Order(orderId++, new Instrument(messageSplit[2]), Long.parseLong(messageSplit[1]), Order.Status.NEW);
        order.setClientKey(key);
        switch (messageSplit[0]) {
            case "11":
                Queues.addOrder(order.getId(), order);
                Queues.addToTMQueue(order);
                break;
            default:

                order.setStatus(Order.Status.FAILED);
                Queues.addToCMQueue(order);
        }
    }
    @Override
    public String outbound(SelectionKey key,Order order)
    {
        System.out.println("Something");
        String message = null;
        if(order.getStatus() == Order.Status.NEW)
        {
            message = "Order received and forwarded to the trader!";
        }
        else if (order.getStatus() == Order.Status.FILLED)
        {
            message = "Order has been filled!";
        }
        else if (order.getStatus() == Order.Status.FAILED)
        {
            message = "Failed to receive the order, please resubmit!";
        }
        return message;
    }
}


