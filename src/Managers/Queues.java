package Managers;

import java.nio.channels.SelectionKey;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Naeem on 06/07/2017.
 */
public class Queues {
    private static ConcurrentLinkedQueue<Order> CMQueue = new ConcurrentLinkedQueue<>();
    private static ConcurrentLinkedQueue<Order> TMQueue = new ConcurrentLinkedQueue<>();
    private static ConcurrentLinkedQueue<Order> RMQueue = new ConcurrentLinkedQueue<>();

    /**
     * Centralised storage for all orders
     */
    private static ConcurrentHashMap<Long,Order> Orders = new ConcurrentHashMap<>();

    public static Order getOrder(long orderid){
        return Orders.get(orderid);
    }
    public static ConcurrentHashMap<Long, Order> getOrderMap()
    {
        return Orders;
    }

    public synchronized static void addOrder(long orderid, Order order){
        if (!Orders.contains(order))
        {
            Orders.put(orderid,order);
        }
    }

    public static ConcurrentLinkedQueue<Order>  getCMQueue()
    {
        return CMQueue;
    }
    public static ConcurrentLinkedQueue<Order>  getTMQueue()
    {
        return TMQueue;
    }
    public static ConcurrentLinkedQueue<Order>  getRMQueue() { return RMQueue; }
    
    public static void addToCMQueue(Order order){
        if (!CMQueue.contains(order))
        {
            CMQueue.add(order);
        }
    }

    public static void addToTMQueue(Order order){
        if (!TMQueue.contains(order))
        {
            TMQueue.add(order);
        }
    }

    public static void addToRMQueue(Order order){
        if (!RMQueue.contains(order))
        {
            RMQueue.add(order);
        }
    }






}
