package Managers;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ParentManager implements Runnable {

    private int port;
    private ConcurrentLinkedQueue<Order> genericHM;


    public ParentManager(int port) {
        this.port = port;
    } //TODO remove

    public ParentManager(int port, ConcurrentLinkedQueue<Order> genericHM) {
        this.port = port;
        this.genericHM = genericHM;
    }

    @Override
    public void run() {
        try {
            Selector selector = Selector.open();
            ServerSocketChannel ssc = ServerSocketChannel.open();
            ssc.configureBlocking(false);
            ssc.bind(new InetSocketAddress("localhost", port));
            ssc.register(selector, SelectionKey.OP_ACCEPT);
            int i = 0, j = 0, k = 0;
            while (true) {
                int readyChannels = selector.select(1000);
                if (readyChannels <= 0) continue;

                Set<SelectionKey> selectionKeys = selector.selectedKeys();
                Iterator<SelectionKey> iterator = selectionKeys.iterator();
                while (iterator.hasNext()) {
                    SelectionKey key = iterator.next();

                    iterator.remove();
                    if (key.isAcceptable()) {
                        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
                        SocketChannel readChannel = serverSocketChannel.accept();
                        readChannel.configureBlocking(false);
                        readChannel.register(key.selector(), SelectionKey.OP_READ, new State());
                        System.out.println("Connected");

                    } else if (key.isReadable()) {
                        System.out.println("Message from the Trader:" + i++);
                        State state = (State) key.attachment();
                        state.numRead += ((SocketChannel) key.channel()).read(state.b);
                        inbound(key, new String(state.b.array()));
                        state.b.flip();
                        state.b.clear();
                        key.channel().register(selector, SelectionKey.OP_WRITE);
                    } else if (key.isWritable()) {
                        for (Order order : genericHM) {
                            if (order.getClientKey().isWritable()) {
                                String message = outbound(key,genericHM.poll());
                                if (message == null) continue;
                                ByteBuffer buffer = ByteBuffer.allocate(message.getBytes().length);
                                buffer.put(message.getBytes());
                                buffer.flip();
                                while (buffer.hasRemaining()) {
                                    ((SocketChannel) key.channel()).write(buffer);
                                }
                            }
                        }
                        key.channel().register(selector, SelectionKey.OP_READ, new State());
                    }

                }
            }
        } catch (Exception e) {
        }
    }

    public void inbound(SelectionKey key, String s) {

    }

    public String outbound(SelectionKey key,Order order) {
        String message = null;
        return message;
    }
}
