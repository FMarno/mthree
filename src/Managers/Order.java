package Managers;

import Ref.Instrument;

import java.nio.channels.SelectionKey;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by James on 7/7/2017.
 */
public class Order
{
	private long id;
	private SelectionKey clientKey;
	private SelectionKey routerKey;
	private SelectionKey traderKey;
	
	private long volume;
	private float price;
    private long volumeRemaining;
	private ArrayList<Slice> slices;
	private ArrayList<Fill> fills;
    private HashMap<SelectionKey,Double> bestPrices;
    private int bestPriceCounter;
	private Instrument instrument;
	private Status status;
	public enum Status {NEW,ACCEPTED,REJECTED,PENDING_FILL,PARTIAL_FILL,FILLED, FAILED, ROUTE_ORDER ,PRICED};
	
	public Order(long id, Instrument instrument, long volume,Status status)
	{
        this.bestPrices = new HashMap<>();
        this.bestPriceCounter=0;
		this.status = status;
		this.id = id;
		this.instrument = instrument;
		this.volume = volume;
	}
	
	//Getters
	public long getId ()
	{
		return id;
	}

	public Status getStatus(){return this.status;}

    public HashMap<SelectionKey, Double> getBestPrices(){return this.bestPrices;}

    public int getBestPriceCounter(){return this.bestPriceCounter;}
	
	public SelectionKey getClientKey ()
	{
		return clientKey;
	}
	
	public SelectionKey getRouterKey ()
	{
		return routerKey;
	}
	
	public SelectionKey getTraderKey ()
	{
		return traderKey;
	}
	
	public long getVolume ()
	{
		return volume;
	}

	public long getVolumeRemaining(){return this.volumeRemaining;}
	
	public float getPrice ()
	{
		return price;
	}
	
	public ArrayList<Slice> getSlices ()
	{
		return slices;
	}
	
	public ArrayList<Fill> getFills ()
	{
		return fills;
	}
	
	public Instrument getInstrument ()
	{
		return instrument;
	}

	
	//Setters
	
	public synchronized void setClientKey (SelectionKey clientKey)
	{
		this.clientKey = clientKey;
	}
	
	public synchronized void setRouterKey (SelectionKey routerKey)
	{
		this.routerKey = routerKey;
	}
	
	public synchronized void setTraderKey (SelectionKey traderKey)
	{
		this.traderKey = traderKey;
	}
	
	public synchronized void setPrice (float price)
	{
		this.price = price;
	}
	
	public synchronized void setSlices (ArrayList<Slice> slices)
	{
		this.slices = slices;
	}
	
	public synchronized void setFills (ArrayList<Fill> fills)
	{
		this.fills = fills;
	}
	
	public synchronized void setStatus (Status status)
	{
		this.status = status;
	}

	//add
    public void add_bestPrice(SelectionKey key, double price){this.bestPrices.put(key,price);}

    public synchronized void addSlice(Slice slice){
        slices.add(slice);
    }

    public void increment_bestPriceCounter(){this.bestPriceCounter++;}

    //fill
    public void createFill(int volume, double price){

    }
}
class Slice
{
	private long id;
	private long volume;
	private boolean complete;
    private HashMap<SelectionKey,Double> bestPrices;
    private int bestPriceCounter;
    private SelectionKey bestRouter;
	
	Slice(long id, long volume)
	{
        this.bestPrices = new HashMap<>();
        this.bestPriceCounter=0;
        this.id = id;
		this.volume = volume;
		complete = false;
	}
	
	long getId()
	{
		return id;
	}
	SelectionKey getBestRouter(){return this.bestRouter;}
	long getVolume()
	{
		return volume;
	}
	boolean getComplete()
	{
		return complete;
	}
}
class Fill
{
	private long id;
	private long volume;
	private float price;
	
	Fill(long id, long volume, float price)
	{
		this.id = id;
		this.volume = volume;
		this.price = price;
	}
	
	long getId()
	{
		return id;
	}
	long getVolume()
	{
		return volume;
	}
	float getPrice()
	{
		return price;
	}
	
}