package Managers;

import java.util.ArrayList;

public class Manager {
	
	public Manager()
	{
		ArrayList<ParentManager> managers = new ArrayList<>();
		ArrayList<Thread> threads = new ArrayList<>();
		
		managers.add(new ClientManager());
		managers.add(new TraderManager());
		managers.add(new RouterManager());
		
		for(ParentManager manager : managers)
		{
			threads.add(new Thread(manager));
		}
		
		for(Thread thread : threads)
		{
			thread.start();
		}
	}
}
