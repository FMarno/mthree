package OrderRouter;

import OrderClient.NewOrderSingle;
import Ref.ConnectToManager;
import Ref.Instrument;
import Ref.Ric;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Random;

/**
 * Created by Naeem on 06/07/2017.
 */
public class OrderRouter extends ConnectToManager implements Router,Runnable{
    private final InetSocketAddress RouterManagerAddress;
    private SocketChannel channel;
    private final int milli,nano;



    public OrderRouter(InetSocketAddress localhost, int milli, int nano) {
        this.RouterManagerAddress = localhost;
        this.milli=milli;
        this.nano=nano;
        try {
            this.channel = connect(this.RouterManagerAddress);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        Random rand = new Random();
        for (int i = 0; i < 1; i++) {
            int size = rand.nextInt();
            float price = rand.nextFloat();
            Instrument instrument = new Instrument(new Ric("FIN.LAY"));
            NewOrderSingle order = new NewOrderSingle(size, price, instrument);
            System.out.println("sending");
            try {
                // TODO: 06/07/2017 The above lines of code should be waiting to recieve data from RouterManager to forward to routeOrder
                int check = this.routeOrder(1,2,10,"InstrumentData");
                if (check == -1){
                    System.err.println("error in newOrder");
                }
                Thread.sleep(milli, nano);
            } catch (IOException | InterruptedException e) {
                System.err.println("Client threw exception");
                e.printStackTrace();
                return;
            }
        }
        while (true){}
    }

    @Override
    public int routeOrder(int id, int sliceId, int size, String instrumentData) throws IOException, InterruptedException {
        if (channel == null && !channel.isConnected()) {
            System.err.println("not connected to Trader Manager");
            return -1;
        }
        //String test = "I am testing this";
        //int messageLen = test.getBytes().length;
        //System.out.println("id=" + id + "|" + order);

        ByteBuffer byteBuffer = ByteBuffer.allocate(instrumentData.getBytes().length);
        byteBuffer.put(instrumentData.getBytes());
        byteBuffer.flip();
        while (byteBuffer.hasRemaining()) {
            channel.write(byteBuffer);
        }
        byteBuffer.clear();

        ByteBuffer responseMessage = ByteBuffer.allocate(48 * 48);
        boolean wait = false;
        int bytesRead = 0;
        String message = null;
        System.out.println("Waiting for msg");
        while (!wait) {
            bytesRead += channel.read(responseMessage);
            message = new String(responseMessage.array());
            if (bytesRead > 0) {
                wait = true;
            }
        }
        System.out.println(message);

        return 0;
    }

    @Override
    public void sendCancel(int id, int sliceId, int size, Instrument i) {

    }

    @Override
    public void priceAtSize(int id, int sliceId, Instrument i, int size) throws IOException {

    }
}
