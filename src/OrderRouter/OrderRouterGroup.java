package OrderRouter;

import java.net.InetSocketAddress;

/**
 * Created by Naeem on 06/07/2017.
 */
public class OrderRouterGroup implements Runnable {
    private final OrderRouter[] routers;
    private final int numRouters;

    public OrderRouterGroup(int numRouters, InetSocketAddress localhost, int milli, int nano) {
        this.numRouters = numRouters;
        this.routers = new OrderRouter[numRouters];
        for(int i=0; i<numRouters;i++) routers[i] = new OrderRouter(localhost,milli,nano);
    }

    @Override
    public void run() {
        for (OrderRouter router : routers) new Thread(router).start();
        System.out.println(this.numRouters + " Started");
    }
}
